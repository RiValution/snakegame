﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Snake.Models;

namespace Snake.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class DirectionController : ControllerBase
    {
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult ChangeDirection([FromBody] Dictionary<string, string> args)
        {
            string directioAsString;
            GameBoard game;
            Direction direction;

            try
            {
                game = GameBoard.GetInstance();
                directioAsString = args["direction"];
                switch (directioAsString.ToLower())
                {
                    case "left":
                        direction = Direction.Left;
                        break;
                    case "top":
                        direction = Direction.Top;
                        break;
                    case "right":
                        direction = Direction.Right;
                        break;
                    case "bottom":
                        direction = Direction.Bottom;
                        break;
                    default:
                        throw new Exception("Invalid direction");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            try
            {
                game.UpdateDirection(direction);
            }
            catch (Exception)
            {
                Console.WriteLine("Inner game exception");
            }

            return Ok();
        }
    }
}
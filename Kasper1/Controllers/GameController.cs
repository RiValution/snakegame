﻿using System;
using Microsoft.AspNetCore.Mvc;
using Snake.Models;

namespace Snake.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        [HttpGet]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(500)]
        public IActionResult GetData()
        {
            try
            {
                var game = GameBoard.GetInstance();
                return Ok(game);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
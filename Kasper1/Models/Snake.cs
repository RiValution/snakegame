﻿using Kasper1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Snake.Models
{
    public class Snake
    {

        public const int StartLength = 2;

        public List<Part> Body { get; private set; } = new List<Part>();

        public Part Head => Body.First();

        public Direction CurrentDirection { get; private set; }

        public Snake(int x, int y, Direction direction = Direction.Top)
        {
            // initialize snake body
            for (int i = 0; i < StartLength; i++)
                Body.Add(new Part(x, y - i));

            CurrentDirection = direction;
        }

        public void UpdateCurrentDirection(Direction direction, out bool enterOppositeDirection)
        {
            // if new direction is opposite, game ends
            enterOppositeDirection = false;
            switch (direction)
            {
                case Direction.Top:
                    enterOppositeDirection = CurrentDirection == Direction.Bottom;
                    CurrentDirection = Direction.Top;
                    break;
                case Direction.Bottom:
                    enterOppositeDirection = CurrentDirection == Direction.Top;
                    CurrentDirection = Direction.Bottom;
                    break;
                case Direction.Right:
                    enterOppositeDirection = CurrentDirection == Direction.Left;
                    CurrentDirection = Direction.Right;
                    break;
                case Direction.Left:
                    enterOppositeDirection = CurrentDirection == Direction.Right;
                    CurrentDirection = Direction.Left;
                    break;
            }

        }

        public void Move(out bool intersectWithBody)
        {
            Part newHead;
            switch (CurrentDirection)
            {
                case Direction.Left:
                    newHead = new Part(Head.X - 1, Head.Y);
                    break;
                case Direction.Bottom:
                    newHead = new Part(Head.X, Head.Y - 1);
                    break;
                case Direction.Right:
                    newHead = new Part(Head.X + 1, Head.Y);
                    break;
                case Direction.Top:
                    newHead = new Part(Head.X, Head.Y + 1);
                    break;
                default:
                    throw new ArgumentException("Invalid arguments");
            }

            // if snake intersects itself, game ends
            intersectWithBody = false;
            if (Body.Contains(newHead))
            {
                intersectWithBody = true;
                return;
            }
         
            // snake moves
            Part part = Body.Last();
            Body.Remove(part);
            Body.Insert(0, newHead);
        }

    }

    public enum Direction
    {
        Top = 0,
        Right = 1,
        Bottom = 2,
        Left = 3
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Game.Models
{
    public class GameBoardSize
    {
        public GameBoardSize(int width, int height) 
        {
            Width = width;
            Height = height;
        }

        private int _width;

        private int _height;

        public int Width
        {
            get => _width;
            private set
            {
                if (value < 2)
                {
                    throw new ArgumentException("Invalid width!");
                }
                _width = value;

            }
        }

        public int Height
        {
            get => _height;
            private set
            {
                if (value < 2 )
                {
                    throw new ArgumentException("Invalid height!");
                }
                _height = value;

            }
        }
    }
}

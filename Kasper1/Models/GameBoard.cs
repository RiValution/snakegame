﻿using System;
using System.Threading;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using Kasper1.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Game.Models;

namespace Snake.Models
{
    public class GameBoard
    {
        private static GameBoard _instance;

        private static readonly object Monitor = new object();

        private int _maxFood = 3;

        private long _timeUntilNextTurnMilliseconds;

        private Timer _timer;

        public GameBoardSize GameBoardSize { get; private set; }

        public int TurnNumber { get; private set; }

        public long TimeUntilNextTurnMilliseconds
        {
            get => _timeUntilNextTurnMilliseconds;
            private set
            {
                if (value < 300)
                {
                    throw new ArgumentException("Invalid timeUntilNextTurnMilliseconds!");
                }
                _timeUntilNextTurnMilliseconds = value;

            }
        }

        public int MaxFood
        {
            get => _maxFood;
            private set
            {
                if (value < 1)
                {
                    throw new ArgumentException("Invalid maxFood!");
                }
                _maxFood = value;

            }
        }

        public Snake Snake { get; private set; }

        public HashSet<Part> Food { get; set; }

        private GameBoard(int width, int height, long turnTimeMs, int maxFood, bool setTimer)
        {
            GameBoardSize = new GameBoardSize(width, height);
            TimeUntilNextTurnMilliseconds = turnTimeMs;
            MaxFood = maxFood;
            Reset();
            if (setTimer)
                _timer = new Timer(new TimerCallback(Update), null, 0, TimeUntilNextTurnMilliseconds);
        }

        public static GameBoard GetInstance(int width = 20, int height = 20, long turnTimeMs = 600, int maxFood = 3, bool setTimer = true)
        {
            if (_instance == null)
            {
                lock (Monitor)
                {
                    if (_instance == null)
                        _instance = new GameBoard(width, height, turnTimeMs, maxFood, setTimer);
                }
            }
            return _instance;
        }

        public static GameBoard Build(int width = 20, int height = 20, long turnTimeMs = 600, int maxFood = 3, bool setTImer = true)
        {
            lock (Monitor)
            {
                _instance = new GameBoard(width, height, turnTimeMs, maxFood, setTImer);
            }
            return _instance;
        }

        public void UpdateDirection(Direction direction)
        {
            Snake.UpdateCurrentDirection(direction, out bool enterOppositeDirection);

            // opposite direction -> user loses
            if (enterOppositeDirection)
            {
                Reset();
            }
        }

        public void Update(object obj)
        {
            // snake has max body -> user wins
            if (Snake.Body.Count >= GameBoardSize.Width * GameBoardSize.Height)
            {
                Reset();
            }

            // if snake grows
            Part tail = Snake.Body.Last();

            TurnNumber++;
            Snake.Move(out bool intersectWithBody);

            // if snake intersects with itself or walls -> user loses
            if (intersectWithBody || IsOutOfBorders(Snake.Head))
            {
                Reset();
            }

            // snake's head on a food cell
            if (Food.Contains(Snake.Head))
            {
                // snake grows
                Snake.Body.Add(tail);
                // update food
                Food.Remove(Snake.Head);
                AddFood();
            }
        }

        private void AddFood()
        {
            Random rnd = new Random();
            // if remains only 2 free positions (neither food nor snake) and foodMax = 3 and Food.Count = 0 -> 
            // -> currentMaxFood = 2
            int currentMaxFood = Math.Min(MaxFood, GameBoardSize.Height * GameBoardSize.Width - Snake.Body.Count) - Food.Count;
            // if MaxFood = 3 and food.Count = 0 -> newFoodCount = {1, 2, 3}
            int newFoodCount = 1 + rnd.Next(currentMaxFood);
            int counter = 0;
            while (counter++ != newFoodCount)
            {
                Part newFood;
                int tries = 0;
                bool isBodyContains;
                do
                {
                    newFood = new Part(rnd.Next(GameBoardSize.Width), rnd.Next(GameBoardSize.Height));
                    isBodyContains = Snake.Body.Contains(newFood);
                }
                while (isBodyContains && tries < 100);

                if (isBodyContains)
                {
                    // alternative method to find food place
                    // it checks 8 cells around snake's head
                    // if food place still not found -> snake will intesects itself or with wall
                    bool foundNewFoodPlace = false;
                    for (int i = Snake.Head.Y - 1; !foundNewFoodPlace && i < Snake.Head.Y + 2; i++)
                    {
                        for (int j = Snake.Head.X - 1;  j < Snake.Head.X + 2; j++)
                        {
                            Part tempPart = new Part(i, j);
                            if (!Snake.Body.Contains(tempPart))
                            {
                                newFood = tempPart;
                                foundNewFoodPlace = true;
                            }
                        }
                    }

                    // game will end in the next turn
                    if (!foundNewFoodPlace)
                    {
                        return;
                    }
                }

                Food.Add(newFood);
            }
        }


        private bool IsOutOfBorders(Part position)
        {
            return
              position.X > GameBoardSize.Width - 1
              || position.Y > GameBoardSize.Height - 1
              || position.X < 0 || position.Y < 0 ? true : false;
        }

        public void Reset()
        {
            TurnNumber = 0;
            Snake = new Snake(GameBoardSize.Width / 2, GameBoardSize.Height / 2);
            Food = new HashSet<Part>();
            AddFood();
        }

        ~GameBoard()
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }
        }


    }
}

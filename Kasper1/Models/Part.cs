﻿using System;
using System.Runtime.Serialization;

namespace Snake.Models
{

    [DataContract]
    public struct Part
    {
     
        [DataMember]
        public int X { get; private set; }
     
        [DataMember]
        public int Y { get; private set; }

        public Part(int x = 0, int y = 0)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(Part cell1, Part cell2)
        {
            return cell1.X == cell2.X && cell1.Y == cell2.Y;
        }

       
        public static bool operator !=(Part c1, Part c2)
        {
            return !(c1 == c2);
        }

        public override bool Equals(object obj)
        {
            return obj is Part cell &&
                   X == cell.X &&
                   Y == cell.Y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }
    }
}


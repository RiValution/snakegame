﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kasper1.Models
{
    public class GameException : Exception
    {
        public GameException(string message)
            : base(string.Format("GameException: {0}", message))
        { }
    }
}

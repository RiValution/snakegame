using System;
using Xunit;
using Snake.Models;


namespace GameTests
{
    public class PartTests
    {
  
        [Theory]
        [InlineData(0, 2)]
        public void InitialPart_ShouldInitializePartWithCoordinates(int x, int y)
        {
            Part part = new Part(x, y);
            Assert.Equal(part.X, x);
            Assert.Equal(part.Y, y);
        }

        [Theory]
        [InlineData(21, 2, true)]
        public void InitialPartsWithTheSameCoordinates_ShouldReturnEqualsTrue(int x, int y, bool expected)
        {
            Part part1 = new Part(x, y);
            Part part2 = new Part(x, y);
            Assert.Equal(part1.Equals(part2), expected);
        }

        [Theory]
        [InlineData(21, 2, true)]
        public void InitialPartsWithTheSameCoordinates_ShouldReturnOperatorEqualsTrue(int x, int y, bool expected)
        {
            Part part1 = new Part(x, y);
            Part part2 = new Part(x, y);
            Assert.Equal(part1 == part2, expected);
        }

        [Theory]
        [InlineData(21, 2, false)]
        public void InitialPartsWithDifferentCoordinates_ShouldReturnOperatorEqualsFalse(int x, int y, bool expected)
        {
            Part part1 = new Part(x, y);
            Part part2 = new Part(x, y + 1);
            Assert.Equal(part1 == part2, expected);
        }
    }
}

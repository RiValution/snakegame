﻿using System;
using Xunit;
using Snake.Models;
using Kasper1.Models;
using System.Linq;

namespace Tests.ModelsTests
{
    public class GameTests
    {
        private object obj = new object();

        [Theory]
        [InlineData(4, 4, 600, 1)]
        public void MoveIntoBorder_ShouldResetSnakeHeadPosition(int x, int y, long time, int maxFood)
        {
            GameBoard game = GameBoard.Build(x, y, time, maxFood, false);
            Part snakeHeadPosition = game.Snake.Body.First();

            game.Update(obj);
            game.Update(obj);

            Assert.Equal(snakeHeadPosition, game.Snake.Body[0]);
        }

        [Theory]
        [InlineData(1, 20, 600, 3, "Invalid width!")]
        [InlineData(20, 1, 600, 3, "Invalid height!")]
        [InlineData(20, 20, 600, 0, "Invalid maxFood!")]
        [InlineData(20, 20, 200, 3, "Invalid timeUntilNextTurnMilliseconds!")]
        public void InittializeGameWithInvalidWith_ShouldThrowArgumentException(int x, int y, long time, int maxFood, string expectedMessage)
        {
            Action act = () => GameBoard.Build(x, y, time, maxFood);
            ArgumentException exception = Assert.Throws<ArgumentException>(act);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData(20, 20, 600, 3, Direction.Top, Direction.Bottom, 0)]
        public void OppositeDirection_ShouldRestartTheGame(int x, int y, long time, int maxFood, Direction direction, Direction oppositeDirection, int expectedTurnNumber)
        {
            GameBoard game = GameBoard.Build(x, y, time, maxFood, false);
            //turn = 1 (One update)
            game.Update(obj);
            game.UpdateDirection(direction);
            game.UpdateDirection(oppositeDirection);
            // Restart -> turn = 0 
            Assert.Equal(expectedTurnNumber, game.TurnNumber);
        }

        [Theory]
        [InlineData(20, 20, 600, 3)]
        public void InitialGameBoard_ShouldGenerateAtLeastOneFoodItem(int x, int y, long time, int maxFood)
        {
            GameBoard game = GameBoard.Build(x, y, time, maxFood, false);
            Assert.True(game.Food.Count >= 1);
        }

        [Theory]
        [InlineData(20, 20, 600, 3)]
        public void SnakeEatFood_ShouldIncreaseSnakeBodyCount(int x, int y, long time, int maxFood)
        {
            GameBoard game = GameBoard.Build(x, y, time, maxFood, false);
            game.Food.Add(new Part(10, 11));
            int previousCount = game.Snake.Body.Count;
            game.Update(obj);
            Assert.Equal(previousCount + 1, game.Snake.Body.Count);
        }

 
    }
}

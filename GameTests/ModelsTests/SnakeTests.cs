﻿using System;
using Xunit;
using Snake.Models;
using System.Linq;
using Kasper1.Models;

namespace Tests.ModelsTests
{
    public class SnakeTests
    {
        private const int width = 10;
        private const int height = 10;
        Snake.Models.Snake snake;

        
        [Theory]
        [InlineData(2, Direction.Top)]
        public void InitialSnake_ShouldHasDefaultParameters(int length, Direction direction)
        {
            snake = new Snake.Models.Snake(width, height);
            Assert.Equal(length, snake.Body.Count);
            Assert.Equal(direction, snake.CurrentDirection);
        }

        [Theory]
        [InlineData(Direction.Left)]
        [InlineData(Direction.Right)]
        [InlineData(Direction.Top)]
        public void UpdateDirectionFromTopToLeft_ShouldChangeCurrentDirectionToLeft(Direction direction)
        {
            snake = new Snake.Models.Snake(width, height);
            snake.UpdateCurrentDirection(direction, out _);
            Assert.Equal(direction, snake.CurrentDirection);
        }

        [Theory]
        [InlineData(Direction.Bottom, true)]
        public void UpdateDirectionFromTopToBottom_ShouldReturnEnterOppositeDirectionTrue(Direction direction, bool expectedValue)
        {
            snake = new Snake.Models.Snake(width, height);
            snake.UpdateCurrentDirection(direction, out bool enterOppositeDirection);
            Assert.Equal(expectedValue, enterOppositeDirection);
        }

        [Theory]
        [InlineData(10, 11)]
        public void InitialSnakeMove_ShouldChangeHeadCoordinatesTo10_11(int x, int y)
        {
            snake = new Snake.Models.Snake(width, height);
            snake.Move(out _);
            Assert.Equal(new Part(x, y), snake.Body.First());
        }

    }
}

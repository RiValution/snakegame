﻿using System;
using Xunit;
using Snake.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;


namespace Tests.ControllersTests
{
   
    public class DirectionControllerTests
    {
      
        private readonly DirectionController _directionController;
        private Dictionary<string, string> _pairs;


        public DirectionControllerTests()
        {
            _directionController = new DirectionController();
        }

        [Fact]
        public void TestSimpleDirectionChange()
        {
            _pairs = new Dictionary<string, string>();
            _pairs.Add("direction", "Right");

            var result = _directionController.ChangeDirection(_pairs);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void TestWrongValueStringDirectionChange()
        {
            _pairs = new Dictionary<string, string>();
            _pairs.Add("direction", "Kitten");

            var result = _directionController.ChangeDirection(_pairs);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void TestWrongKeyStringDirectionChange()
        {
            _pairs = new Dictionary<string, string>();
            _pairs.Add("Kit", "Kitten");

            var result = _directionController.ChangeDirection(_pairs);
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
